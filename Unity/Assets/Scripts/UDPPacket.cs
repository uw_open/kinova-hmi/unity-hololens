﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

namespace UnityUDP
{
    /// <summary>
    /// Udp Packet
    /// </summary>
    abstract public class UDPPacket
    {
        #region Member Variables
        protected List<object> _data;
        protected byte[] _binaryData;
        protected string _address;
        protected long _timeStamp;
        #endregion

        #region Properties
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
#if !NETFX_CORE
                Trace.Assert(string.IsNullOrEmpty(_address) == false);
#endif
                _address = value;
            }
        }

        public List<object> Data
        {
            get
            {
                return _data;
            }
        }

        public byte[] BinaryData
        {
            get
            {
                Pack();
                return _binaryData;
            }
        }

        public long TimeStamp
        {
            get
            {
                return _timeStamp;
            }
            set
            {
                _timeStamp = value;
            }
        }
        #endregion

        #region Methods
        abstract public bool IsBundle();
        abstract public void Pack();
        abstract public void Append<T>(T msgvalue);

        /// <summary>
        /// UDP Packet initialization.
        /// </summary>
        public UDPPacket()
        {
            this._data = new List<object>();
        }

        /// <summary>
        /// Swap endianess given a data set.
        /// </summary>
        /// <param name="data">
        /// A <see cref="System.Byte[]"/>
        /// </param>
        /// <returns>
        /// A <see cref="System.Byte[]"/>
        /// </returns>
        public static byte[] SwapEndian(byte[] data)
        {
            byte[] swapped = new byte[data.Length];
            for (int i = data.Length - 1, j = 0; i >= 0; i--, j++)
            {
                swapped[j] = data[i];
            }
            return swapped;
        }

        /// <summary>
        /// Packs a value in a byte stream. Accepted types: Int32, Int64, Single, Double, String and Byte[].
        /// </summary>
        /// <param name="value">
        /// A <see cref="T"/>
        /// </param>
        /// <returns>
        /// A <see cref="System.Byte[]"/>
        /// </returns>
        //@see https://github.com/jorgegarcia/UnityUDP/issues/8
        //changed by littlewing
        //addaptive for iOS
        public static byte[] PackValue<T>(T value)
        {
            object valueObject = value;
            byte[] data = null;

            if (value is int)
            {
                data = BitConverter.GetBytes((int)valueObject);
                //if (BitConverter.IsLittleEndian) data = SwapEndian(data);
            }
            else if (value is long)
            {
                data = BitConverter.GetBytes((long)valueObject);
                //if (BitConverter.IsLittleEndian) data = SwapEndian(data);
            }
            else if (value is float)
            {
                data = BitConverter.GetBytes((float)valueObject);
                //if (BitConverter.IsLittleEndian) data = SwapEndian(data);
            }
            else if (value is double)
            {
                data = BitConverter.GetBytes((double)valueObject);
                //if (BitConverter.IsLittleEndian) data = SwapEndian(data);
            }
            else if (value is string)
            {
                data = Encoding.ASCII.GetBytes((string)valueObject);
            }
            else if (value is byte[])
            {
                byte[] valueData = ((byte[])valueObject);
                List<byte> bytes = new List<byte>();
                bytes.AddRange(PackValue(valueData.Length));
                bytes.AddRange(valueData);
                data = bytes.ToArray();
            }
            else
            {
                throw new Exception("Unsupported data type.");
            }
            return data;
        }

        /// <summary>
        /// Unpacks a value from a byte stream. Accepted types: Int32, Int64, Single, Double, String and Byte[].
        /// </summary>
        /// <param name="data">
        /// A <see cref="System.Byte[]"/>
        /// </param>
        /// <param name="start">
        /// A <see cref="System.Int32"/>
        /// </param>
        /// <returns>
        /// A <see cref="T"/>
        /// </returns>
        public static T UnpackValue<T>(byte[] data, ref int start)
        {
            object msgvalue; //msgvalue is casted and returned by the function
            Type type = typeof(T);
            byte[] buffername;

            if (type.Name == "String")
            {
                int count = 0;
                for (int index = start; data[index] != 0; index++) count++;

                msgvalue = Encoding.ASCII.GetString(data, start, count);
                start += count + 1;
                start = ((start + 3) / 4) * 4;
            }
            else if (type.Name == "Byte[]")
            {
                int length = UnpackValue<int>(data, ref start);
                byte[] buffer = new byte[length];
                Array.Copy(data, start, buffer, 0, buffer.Length);
                start += buffer.Length;
                start = ((start + 3) / 4) * 4;

                msgvalue = buffer;
            }
            else
            {
                switch (type.Name)
                {
                    case "Int32":
                    case "Single"://this also serves for float numbers
                        buffername = new byte[4];
                        break;

                    case "Int64":
                    case "Double":
                        buffername = new byte[8];
                        break;

                    default:
                        throw new Exception("Unsupported data type.");
                }

                Array.Copy(data, start, buffername, 0, buffername.Length);
                start += buffername.Length;

                if (BitConverter.IsLittleEndian)
                {
                    //buffername = SwapEndian(buffername);
                }

                switch (type.Name)
                {
                    case "Int32":
                        msgvalue = BitConverter.ToInt32(buffername, 0);
                        break;

                    case "Int64":
                        msgvalue = BitConverter.ToInt64(buffername, 0);
                        break;

                    case "Single":
                        msgvalue = BitConverter.ToSingle(buffername, 0);
                        break;

                    case "Double":
                        msgvalue = BitConverter.ToDouble(buffername, 0);
                        break;

                    default:
                        throw new Exception("Unsupported data type.");
                }
            }

            return (T)msgvalue;
        }

        /// <summary>
		/// Unpacks an array of binary data.
		/// </summary>
		/// <param name="data">
		/// A <see cref="System.Byte[]"/>
		/// </param>
		/// <returns>
		/// A <see cref="UDPPacket"/>
		/// </returns>
        /*
		public static UDPPacket Unpack(byte[] data)
        {
            int start = 0;
            return Unpack(data, ref start, data.Length);
        }
        */
        /// <summary>
        /// Unpacks an array of binary data given reference start and end pointers.
        /// </summary>
        /// <param name="data">
        /// A <see cref="System.Byte[]"/>
        /// </param>
        /// <param name="start">
        /// A <see cref="System.Int32"/>
        /// </param>
        /// <param name="end">
        /// A <see cref="System.Int32"/>
        /// </param>
        /// <returns>
        /// A <see cref="UDPPacket"/>
        /// </returns>
        /*
        public static UDPPacket Unpack(byte[] data, ref int start, int end)
        {
            if (data[start] == '#')
            {
                return UDPBundle.Unpack(data, ref start, end);
            }

            else return UDPMessage.Unpack(data, ref start);
        }
        */
        /// <summary>
        /// Pads null a list of bytes.
        /// </summary>
        /// <param name="data">
        /// A <see cref="List<System.Byte>"/>
        /// </param>
        public static void PadNull(List<byte> data)
        {
            byte nullvalue = 0;
            int pad = 4 - (data.Count % 4);
            for (int i = 0; i < pad; i++)
                data.Add(nullvalue);
        }

        #endregion
    }
}