﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.IO;
using UnityUDP;
using ElencySolutions.CsvHelper;
using System.Globalization;

public class GenerateSlot : MonoBehaviour
{
    // qd file name
    public string file_name = "qd_sumsines_random";

    // Preview - show the scrolling slot
    public bool preview = true;

    // Preview - amount of preview
    public float previewTime = 0; // seconds

    // Reference position of holograms
    private Vector3 refPos;

    // the length of segment (world space)
    private float SegmentLength = 40f;
    
    // the segment resolution (number of horizontal points)
    public int SegmentResolution = 160;
    
    // the prefab including MeshFilter and MeshRenderer
    public MeshFilter SegmentPrefab;

    public float _scaley = 0.2f;

    public float gapWidth = 0.02f;

    public float Phase = 0;

    // helper array to generate new segment without further allocations
    private Vector3[] _vertexArray;

    // the pool of free mesh filters
    private List<MeshFilter> _freeMeshFilters = new List<MeshFilter>();

    // Counter for moving the segments automatically
    int counter = 0;

    private bool isRunning = false;

    // Base Gameobject
    private GameObject holobase, movingFrame, connect, refFrame, topMask;
    //private Slider gapSlider, speedSlider;

    private float currentPosition, targetTrajectory, _qd_fs_lut = 100f;

    private List<Single> _desired_traj;
    private CsvFile qd_file;

    
    public float TargetTrajectory
    {
        get
        {
            return GetDesiredTrajectory(currentPosition);//(GetDepth(currentPosition) + GetHeight(currentPosition)) / 2f;
        }
    }

    public float SegLength
    {
        get
        {
            return (float)SegmentLength;
        }
    }

    void Awake()
    {
        currentPosition = 0;
        movingFrame = GameObject.Find("MovingFrame");
        holobase = GameObject.Find("Holograms");
        connect = GameObject.Find("Connect");
        refFrame = GameObject.Find("RefFrame");
        topMask = GameObject.Find("PreviewMask");
        //gapSlider = GameObject.Find("Canvas").transform.GetChild(1).GetComponent<Slider>();
        //speedSlider = GameObject.Find("Canvas").transform.GetChild(0).GetComponent<Slider>();

        // Initialize the reference position of the holograms
        refPos = holobase.transform.position;

        // Load the desired trajectory
        _desired_traj = new List<Single>();
        qd_file = new CsvFile();

        var asset = Resources.Load(file_name) as TextAsset;

        //Debug.Log(asset.text);
        var memstream = new MemoryStream(asset.bytes);
        var streamReader = new StreamReader(memstream);

        qd_file.Populate(streamReader.BaseStream, true);
        //Debug.Log(String.Format("Number of records: {0}", qd_file.RecordCount));
        foreach (var record in qd_file.Records)
        {
            //Debug.Log(String.Format("Number of fields: {0}", record.FieldCount));
            foreach (var field in record.Fields)
            {
                //Debug.Log(String.Format("Field Length: {0}", field.Length));
                _desired_traj.Add(float.Parse(field, CultureInfo.InvariantCulture.NumberFormat));
                //Debug.Log(String.Format("_desired_traj_: {0} | {1}", field, float.Parse(field, CultureInfo.InvariantCulture.NumberFormat)));
            }
        }
        SegmentLength = qd_file.RecordCount / _qd_fs_lut;
        SegmentResolution = qd_file.RecordCount;
        //Debug.Log(String.Format("qd_file: {0}", _desired_traj.ToArray().Length));

        // Create vertex array helper
        _vertexArray = new Vector3[SegmentResolution * 2];
        
        // Build triangles array. For all meshes this array always will
        // look the same, so I am generating it once 
        int iterations = _vertexArray.Length / 2 - 1;
        var triangles = new int[(_vertexArray.Length - 2) * 3];
        
        for (int i = 0; i < iterations; ++i)
        {
            int i2 = i * 6;
            int i3 = i * 2;
            
            triangles[i2] = i3 + 2;
            triangles[i2 + 1] = i3 + 1;
            triangles[i2 + 2] = i3 + 0;
            
            triangles[i2 + 3] = i3 + 2;
            triangles[i2 + 4] = i3 + 3;
            triangles[i2 + 5] = i3 + 1;
        }
        
        // Create colors array. For now make it all white.
        var colors = new Color32[_vertexArray.Length];
        for (int i = 0; i < colors.Length; ++i)
        {
            colors[i] = new Color32(255, 255, 255, 255);
        }

        // Create game objects (with MeshFilter) instances.
        // Assign vertices, triangles, deactivate and add to the pool.
        MeshFilter filter = Instantiate(SegmentPrefab);

        Mesh mesh = filter.mesh;
        mesh.Clear();

        mesh.vertices = _vertexArray;
        mesh.triangles = triangles;
        if (preview)
        {
            GenerateSegment(0, ref mesh);

            filter.mesh = mesh;
            _freeMeshFilters.Add(filter);
            filter.transform.Rotate(new Vector3(0, 1, 0), -90f);
            filter.transform.SetParent(movingFrame.transform);
            movingFrame.transform.GetChild(0).localPosition = new Vector3(0, 0, 0);
            movingFrame.transform.GetChild(0).localScale = new Vector3(1, 1, 1);

            // Set gap width of masks to show desired amount of preview
            topMask.transform.position = topMask.transform.position - topMask.transform.forward * previewTime * 0.5f;
        }
        else
        {
            topMask.transform.localScale = new Vector3(0, 0, 0);
            topMask.GetComponent<MeshRenderer>().enabled = false;
        }
    }
    
    void Update()
    {
       //
    }

    /// <summary>
    ///  Load the Desired Trajectory from file
    /// </summary>
    /// <param name="filename"></param>
    /// <returns></returns>
    public float GetDesiredTrajectory(float currentTime)
    {
        int lenx = _desired_traj.ToArray().Length;
        float Tp = lenx / _qd_fs_lut;

        float iq = ((currentTime % Tp) / (Tp - (1 / _qd_fs_lut))) * ((float)lenx - 1);
        int i0 = Mathf.FloorToInt(iq);
        int i1 = Mathf.CeilToInt(iq);

        if (i1 > (lenx - 1))
            i1 = i0;

        return (float)(_scaley * (_desired_traj.ToArray()[i0] + (iq - (float)i0) * (_desired_traj.ToArray()[i1] - _desired_traj.ToArray()[i0])));
    }

    // This function generates a mesh segment.
    // Index is a segment index (starting with 0).
    // Mesh is a mesh that this segment should be written to.
    public void GenerateSegment(int index, ref Mesh mesh)
    {
        float startPosition = 0;
        float step = SegmentLength / (SegmentResolution - 1);
        
        for (int i = 0; i < SegmentResolution; ++i)
        {
            // get the relative x position
            float xPos = step * i;
            currentPosition = startPosition + xPos;

            float yPosTop = GetDesiredTrajectory(currentPosition); // position passed to GetHeight() must be absolute

            _vertexArray[i * 2] = new Vector3((yPosTop + gapWidth/2f), xPos, 0);

            // bottom vertex always at y=0
            _vertexArray[i * 2 + 1] = new Vector3(yPosTop - gapWidth/2f, xPos, 0);
        }
        
        mesh.vertices = _vertexArray;
        
        // need to recalculate bounds, because mesh can disappear too early
        mesh.RecalculateBounds();
    }
    
    private struct Segment
    {
        public int Index { get; set; }
        public MeshFilter MeshFilter { get; set; }
    }
}
