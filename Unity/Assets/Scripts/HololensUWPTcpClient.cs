﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityUDP;
using Vuforia;
using UnityEngine.XR.WSA.WebCam;
using System.Linq;

#if !UNITY_EDITOR
using System.Threading.Tasks;  
#endif

namespace HoloToolkit.Unity.InputModule
{
    public class HololensUWPTcpClient : MonoBehaviour, IInputClickHandler
    {

#if !UNITY_EDITOR
        private bool _useUWP = true;
        private Windows.Networking.Sockets.StreamSocket socket;
        private Task exchangeTask;
        private Stream streamIn, streamOut;
#endif

#if UNITY_EDITOR
        private bool _useUWP = false;
        System.Net.Sockets.TcpClient client;
        System.Net.Sockets.NetworkStream stream;
        private Thread exchangeThread;
#endif
        private const int HEADER_LENGTH = 6;
        private byte[] _headerBytes = new byte[2] { 59, 57 };
        private List<byte> toSend;
        private List<float> _data;
        private byte[] _sendBytes;
        private float[] targetPosition, jointPositions, robotPosition;
        private float currentTime;

        private StreamWriter writer;
        private StreamReader reader;
        public string _host = "10.158.165.145";
        public int portNb = 11000;
        public Color enabledColor = new Color(0, 1, 0);
        public Color disabledColor = new Color(1, 0, 0);

        private GameObject connectButton, startButton, target;
        public bool toggleConnection = false; 
        private bool isRunning = false;
        private const int headerBufferSize = 6;
        private const int recvBufferSize = 10;
        private bool isConnected;
        private VideoCapture m_VideoCapture = null;

        public bool IsConnected
        {
            get
            {
                return isConnected;
            }
        }

        public float CurrentTime
        {
            get
            {
                return currentTime;
            }
        }

        private void Start()
        {
            _data = new List<float>();
            toSend = new List<byte>();
            connectButton = GameObject.Find("Connect");
            startButton = GameObject.Find("Start");
            target = GameObject.Find("Target");
            MeshRenderer connectRender = connectButton.transform.GetComponentInChildren<MeshRenderer>();
            connectRender.material.color = disabledColor;
            isRunning = false;
            targetPosition = new float[2] { 0, 0 };
            robotPosition = new float[2] { 0, 0 };
            jointPositions = new float[4] { 0, 0, 0, 0 };
            isConnected = false;
            //toggleConnection = true; // DEBUG
            //HandleClicker(); // DEBUG
            //VideoCapture.CreateAsync(false, OnVideoCaptureCreated);
        }

        void OnVideoCaptureCreated(VideoCapture videoCapture)
        {
            if (videoCapture != null)
            {
                m_VideoCapture = videoCapture;

                Resolution cameraResolution = VideoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).First();
                float cameraFramerate = VideoCapture.GetSupportedFrameRatesForResolution(cameraResolution).OrderByDescending((fps) => fps).First();

                CameraParameters cameraParameters = new CameraParameters();
                cameraParameters.hologramOpacity = 0.0f;
                cameraParameters.frameRate = cameraFramerate;
                cameraParameters.cameraResolutionWidth = cameraResolution.width;
                cameraParameters.cameraResolutionHeight = cameraResolution.height;
                cameraParameters.pixelFormat = CapturePixelFormat.BGRA32;

                m_VideoCapture.StartVideoModeAsync(cameraParameters,
                                                    VideoCapture.AudioState.None,
                                                    OnStartedVideoCaptureMode);
            }
            else
            {
                Debug.LogError("Failed to create VideoCapture Instance!");
            }
        }

        void OnStartedVideoCaptureMode(VideoCapture.VideoCaptureResult result)
        {
            if (result.success)
            {
                string filename = string.Format("EXPT_{0}.mp4", Time.time);
                string filepath = System.IO.Path.Combine(Application.persistentDataPath, filename);

                m_VideoCapture.StartRecordingAsync(filepath, OnStartedRecordingVideo);
            }
        }

        void OnStartedRecordingVideo(VideoCapture.VideoCaptureResult result)
        {
            Debug.Log("Started Recording Video!");
            // We will stop the video from recording via other input such as a timer or a tap, etc.
        }

        // The user has indicated to stop recording
        void StopRecordingVideo()
        {
            m_VideoCapture.StopRecordingAsync(OnStoppedRecordingVideo);
        }

        void OnStoppedRecordingVideo(VideoCapture.VideoCaptureResult result)
        {
            Debug.Log("Stopped Recording Video!");
            m_VideoCapture.StopVideoModeAsync(OnStoppedVideoCaptureMode);
        }

        void OnStoppedVideoCaptureMode(VideoCapture.VideoCaptureResult result)
        {
            m_VideoCapture.Dispose();
            m_VideoCapture = null;
        }

        public virtual void OnInputClicked(InputClickedEventData eventData)
        {
            // On each tap gesture, toggle whether the experiment is running or not
            toggleConnection = !toggleConnection;
            HandleClicker();
            eventData.Use();
        }

        private void HandleClicker()
        {
            if (toggleConnection && !isConnected)
            {
                // Stop Vuforia
                //UnityEngine.XR.XRSettings.enabled = false;
                //VuforiaBehaviour.Instance.enabled = false;
                // Start MRC Video Capture
                
                try
                {
                    Connect(_host, portNb.ToString());
                    //Debug.Log("Connected to Robot Controller at " + host);
                }
                catch (System.Exception e)
                {
                    Debug.Log("Cannot connect Robot Controller: " + e.ToString());
                }
            }
            else
            {
                StopRecordingVideo();
                OnDestroy();
            }
                
        }

        public void Connect(string host, string port)
        {
            if (_useUWP)
            {
                ConnectUWP(host, port);
            }
            else
            {
                ConnectUnity(host, port);
            }
        }



#if UNITY_EDITOR
        private void ConnectUWP(string host, string port)
#else
        private async void ConnectUWP(string host, string port)
#endif
        {
#if UNITY_EDITOR
            errorStatus = "UWP TCP client used in Unity!";
#else
            try
            {
                if (exchangeTask != null) StopExchange();

                socket = new Windows.Networking.Sockets.StreamSocket();
                Windows.Networking.HostName serverHost = new Windows.Networking.HostName(host);
                await socket.ConnectAsync(serverHost, port);

                streamOut = socket.OutputStream.AsStreamForWrite();
                writer = new StreamWriter(streamOut) { AutoFlush = true };

                streamIn = socket.InputStream.AsStreamForRead();
                reader = new StreamReader(streamIn, true);

                RestartExchange();
                successStatus = "Connected!";
                isConnected = true;
            }
            catch (Exception e)
            {
                isConnected = false;
                errorStatus = e.ToString();
            }
#endif
        }

        private void ConnectUnity(string host, string port)
        {
#if !UNITY_EDITOR
            errorStatus = "Unity TCP client used in UWP!";
#else
            try
            {
                if (exchangeThread != null) StopExchange();

                client = new System.Net.Sockets.TcpClient(host, Int32.Parse(port));
                stream = client.GetStream();
                reader = new StreamReader(stream);
                writer = new StreamWriter(stream) { AutoFlush = true };

                RestartExchange();
                successStatus = "Connected!";
                isConnected = true;
            }
            catch (Exception e)
            {
                isConnected = false;
                errorStatus = e.ToString();
            }
#endif
        }

        private bool exchanging = false;
        private bool exchangeStopRequested = false;
        private string lastPacket = null;

        private string errorStatus = null;
        private string warningStatus = null;
        private string successStatus = null;
        private string unknownStatus = null;
        
        #region Properties
        public List<float> Data
        {
            get
            {
                return _data;
            }
        }

        public float[] TargetPosition
        {
            get
            {
                return targetPosition;
            }
        }

        public float[] RobotPosition
        {
            get
            {
                return robotPosition;
            }
        }

        public float[] JointPositions
        {
            get
            {
                return jointPositions;
            }
        }

        public bool IsRunning
        {
            get
            {
                return isRunning;
            }
        }
        #endregion

        public void RestartExchange()
        {
#if UNITY_EDITOR
            if (exchangeThread != null) StopExchange();
            exchangeStopRequested = false;
            exchangeThread = new System.Threading.Thread(ExchangePackets);
            exchangeThread.Start();
#else
            if (exchangeTask != null) StopExchange();
            exchangeStopRequested = false;
            exchangeTask = Task.Run(() => ExchangePackets());
#endif
        }

        public void Update()
        {
            //isRunning = startButton.GetComponentInChildren<StartStopScript>().IsRunning;
            //Debug.Log(String.Format("isRunning: {0}", isRunning));
            if (isConnected)
            {
                MeshRenderer connectRender = connectButton.transform.GetComponentInChildren<MeshRenderer>();
                connectRender.material.color = enabledColor;
                //Vuforia.VuforiaBehaviour.Instance.enabled = false;
            }
            else
            {
                MeshRenderer connectRender = connectButton.transform.GetComponentInChildren<MeshRenderer>();
                connectRender.material.color = disabledColor;
            }

            if (lastPacket != null)
            {
                //Debug.Log(lastPacket);//ReportDataToTrackingManager(lastPacket);
            }

            if (errorStatus != null)
            {
                Debug.LogError(errorStatus);
                errorStatus = null;
                HandleClicker();
            }
            if (warningStatus != null)
            {
                Debug.LogWarning(warningStatus);
                warningStatus = null;
            }
            if (successStatus != null)
            {
                Debug.Log(successStatus);
                successStatus = null;
            }
            if (unknownStatus != null)
            {
    
                unknownStatus = null;
            }
            
        }

        /// <summary>
        /// Function to pack float array into byte array and then to string
        /// </summary>
        private void Pack()
        {
            /// <summary>
            /// Packet header is (in this case): headerID (59,57), dataSize (WORD), packetsLeft (WORD) but not used here
            /// header=string.char(59,57,math.mod(#data,256),math.floor(#data/256),0,0)
            /// </summary>
            byte[] header = new byte[HEADER_LENGTH];
            int packetLength = (sizeof(float) * _data.ToArray().Length);
            ushort s = (ushort)packetLength;
            ushort packetsLeft = (ushort)0;
            byte[] dataSize = new byte[2];
            dataSize = BitConverter.GetBytes(s);
            byte[] packetsLeft_bytes = new byte[2];
            packetsLeft_bytes = BitConverter.GetBytes(packetsLeft);
            
            header[0] = _headerBytes[0];
            header[1] = _headerBytes[1];
            header[2] = dataSize[0];
            header[3] = dataSize[1];
            header[4] = (packetsLeft_bytes)[0];
            header[5] = (packetsLeft_bytes)[1];
            //Debug.Log(String.Format("header: {0}-{1}", header[0], header[1]));
            //Debug.Log(String.Format("dataSize: {0}", (byte)s));
            //Debug.Log(String.Format("packetsLeft_bytes: {0}", (byte)packetsLeft));

            // Add header and packet to send
            toSend.Clear();
            toSend.AddRange(header);
            //for (int i=0; i<HEADER_LENGTH; i++)
            //    toSend.Add(header[i]);
            for (int i=0; i<_data.ToArray().Length; i++)
                toSend.AddRange(UDPPacket.PackValue(_data[i]));

            //Debug.Log(String.Format("Sending Data: {0}", UnPack(UDPPacket.PackValue(_data[0]))[0]));
            _sendBytes = toSend.ToArray();

            //Debug.Log(String.Format("toSend: {0}", _sendBytes));

        }

        /// <summary>
        /// Function to unpack float array from byte array
        /// </summary>
        private float[] UnPack(byte[] data)
        {
            int start = 0;
            float[] dataFloat = new float[data.Length / sizeof(float)];

            for (int i=0; i<dataFloat.Length; i++)
            {
                dataFloat[i] = UDPPacket.UnpackValue<Single>(data, ref start);
            }
            return (dataFloat);
        }


        /// <summary>
        /// Following function reads data from the socket(only single packet data for simplicity sake):
        /// </summary>
        private byte[] ReadSocketData()
        {
            // Packet header is: headerID(59,57), dataSize(WORD), packetsLeft(WORD) but not used here
            string received = null;

            int recv = 0;
            byte[] bytes = new byte[HEADER_LENGTH];

#if UNITY_EDITOR
            recv = stream.Read(bytes, 0, HEADER_LENGTH);
#else
            recv = streamIn.Read(bytes, 0, HEADER_LENGTH);
#endif

            received += Encoding.ASCII.GetString(bytes, 0, recv);

            /*Debug.Log(String.Format("header: {0}-{1}-{2}-{3}-{4}-{5}, length: {6}", Encoding.ASCII.GetBytes(received)[0]
                                                                     , Encoding.ASCII.GetBytes(received)[1]
                                                                     , Encoding.ASCII.GetBytes(received)[2]
                                                                     , Encoding.ASCII.GetBytes(received)[3]
                                                                     , Encoding.ASCII.GetBytes(received)[4]
                                                                     , Encoding.ASCII.GetBytes(received)[5]
                                                                     , received.Length));*/
            if (received == null)
                return (null);
            int l = 0;
            if (Encoding.ASCII.GetBytes(received)[0] == _headerBytes[0] && Encoding.ASCII.GetBytes(received)[1] == _headerBytes[1])
            {
                l = Encoding.ASCII.GetBytes(received)[2] + Encoding.ASCII.GetBytes(received)[3] * 256;
                bytes = new byte[l];
#if UNITY_EDITOR
                recv = stream.Read(bytes, 0, l);
#else
                recv = streamIn.Read(bytes, 0, l);
#endif
                //Debug.Log(String.Format("dataLength: {0}-{1}-{2}",l,recv,bytes.Length));
                return bytes;
            }
            else
            {
                Debug.Log(String.Format("Wrong Header! (received-actual): {0}-{1}, {2}-{3}", Encoding.UTF8.GetBytes(received)[0]
                                                                                           , _headerBytes[0]
                                                                                           , Encoding.UTF8.GetBytes(received)[1]
                                                                                           , _headerBytes[1]));
                return null;
            }
        }

        public void ExchangePackets()
        {
            while (!exchangeStopRequested)
            {
                if (writer == null || reader == null) continue;
                exchanging = true;

                /* Write data to socket
                _data.Clear();
                if (isRunning)
                    _data.Add(1.0f);
                else
                    _data.Add(0.0f);
                Pack();
                stream.Write(_sendBytes, 0, _sendBytes.Length);
                //Debug.Log(String.Format("Sending : {0}", _sendBytes[9]));
                */

                // Read data from the controller
                byte[] received = ReadSocketData();
                if (received == null)
                {
                    isConnected = false;
                    continue;
                }
                lastPacket = Encoding.ASCII.GetString(received);

                exchanging = false;

                // Unpack the float values from the received data
                float[] receivedF = UnPack(received);
                /*Debug.Log(String.Format("received (length; bytes): {0}; {1}-{2}-{3}-{4}", received.Length,
                                                                                          received[0],
                                                                                          received[1],
                                                                                          received[2],
                                                                                          received[3]));*/
                isRunning = Convert.ToBoolean(receivedF[0]);
                //Debug.Log(string.Format("isRunning: {0}", isRunning));
                currentTime = receivedF[1];
                //targetPosition[0] = receivedF[2];
                //targetPosition[1] = receivedF[2];
                //robotPosition[0] = receivedF[3];
                //robotPosition[1] = receivedF[4];
                /*
                jointPositions[0] = receivedF[5];
                jointPositions[1] = receivedF[6];
                jointPositions[2] = receivedF[7];
                jointPositions[3] = receivedF[8];
                */
                //Debug.Log(String.Format("Joint1: {0}, Joint2: {1}, Joint3: {2}, Joint4: {3}", jointPositions[0], jointPositions[1], jointPositions[2], jointPositions[3]));
            }
            Debug.Log("Exchange stop requested!");
        }
       
        public void StopExchange()
        {
            exchangeStopRequested = true;

#if UNITY_EDITOR
            if (exchangeThread != null)
            {
                exchangeThread.Abort();
                stream.Close();
                client.Close();
                writer.Close();
                reader.Close();

                stream = null;
                exchangeThread = null;
            }
#else
            if (exchangeTask != null) {
                exchangeTask.Wait();
                socket.Dispose();
                writer.Dispose();
                reader.Dispose();

                socket = null;
                exchangeTask = null;
            }
#endif
            writer = null;
            reader = null;
        }

        public void OnDestroy()
        {
            StopExchange();
        }

    }
}