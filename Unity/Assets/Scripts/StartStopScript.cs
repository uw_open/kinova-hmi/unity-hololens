﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

namespace HoloToolkit.Unity.InputModule
{
    public class StartStopScript : MonoBehaviour, IInputClickHandler
    {
        private GameObject button, target, refPos, status, connect, robot, movingFrame, slot, holobase, imgtgt;
        private GameObject[] joint;
        public Color enabledColor = new Color(0, 1, 0);
        public Color disabledColor = new Color(1, 0, 0);
        private Quaternion initRot;
        private Vector3 startPosition;
        private float startTime;
        private bool isRunning, doUpdate;
        private float[] targetPosition, prevAngle, newAngle, endEffectorPos;
        private string[] robotJoints = new string[4] { "m1n4s200_link_1 (continuous Joint: m1n4s200_joint_1)",
                                                       "m1n4s200_link_2 (revolute Joint: m1n4s200_joint_2)",
                                                       "m1n4s200_link_3 (revolute Joint: m1n4s200_joint_3)",
                                                       "m1n4s200_link_4 (continuous Joint: m1n4s200_joint_4)" };
        private Vector3[] robotInitialState, _jointaxis;
        private StreamWriter writer;

        public bool IsRunning
        {
            get
            {
                return isRunning;
            }
        }

        // Use this for initialization
        void Start()
        {
            status = GameObject.Find("Managers");
            button = GameObject.Find("Start").transform.GetChild(0).gameObject;
            target = GameObject.Find("Target");
            refPos = GameObject.Find("RefFrame");
            connect = GameObject.Find("Connect");
            //robot = GameObject.Find("RobotPosition");
            movingFrame = GameObject.Find("MovingFrame");
            holobase = GameObject.Find("Holograms");
            slot = GameObject.Find("Slot");
            imgtgt = GameObject.Find("ImageTarget");
            initRot = holobase.transform.rotation;
            
            joint = new GameObject[4];
            robotInitialState = new Vector3[4];
            //target.gameObject.SetActive(false);
            /*
            _jointaxis = new Vector3[4];
            for (int i = 0; i < joint.Length; i++)
            {
                joint[i] = GameObject.Find(robotJoints[i]);
            }
            prevAngle = new float[4] { 0, 0, 0, 0 };
            newAngle = new float[4] { 0, 0, 0, 0 };
            _jointaxis[0] = new Vector3(0, 1, 0);
            _jointaxis[1] = new Vector3(0, 1, 0);
            _jointaxis[2] = new Vector3(0, 1, 0);
            _jointaxis[3] = new Vector3(0, 1, 0);
            */
            doUpdate = false;
            startPosition = refPos.transform.position;
            targetPosition = connect.GetComponent<HololensUWPTcpClient>().TargetPosition;
            endEffectorPos = connect.GetComponent<HololensUWPTcpClient>().RobotPosition;
            isRunning = connect.GetComponent<HololensUWPTcpClient>().IsRunning;
        }

        // Update is called once per frame
        void Update()
        {
            //Debug.Log(string.Format("initRot: [{0},{1},{2},{3}]", initRot.x, initRot.y, initRot.z, initRot.w));
            //isRunning = connect.GetComponent<HololensUWPTcpClient>().IsConnected;
            HandleRunning();
        }

        public virtual void OnInputClicked(InputClickedEventData eventData)
        {
            // On each tap gesture, toggle whether the experiment is running or not
            //isRunning = !isRunning;
            HandleRunning();
            eventData.Use();
        }

        private void HandleRunning()
        {
            startPosition = refPos.transform.position;
            bool prevRunning = isRunning;
            isRunning = connect.GetComponent<HololensUWPTcpClient>().IsRunning;
            if ((prevRunning != isRunning) && isRunning)
                startTime = Time.time;
            targetPosition = connect.GetComponent<HololensUWPTcpClient>().TargetPosition;
            endEffectorPos = connect.GetComponent<HololensUWPTcpClient>().RobotPosition;
            /*
            for (int i = 0; i < joint.Length; i++)
            {
                newAngle[i] = connect.GetComponent<HololensUWPTcpClient>().JointPositions[i];
                /*
                if (Vector4.Magnitude(new Vector4(newAngle[0] - prevAngle[0], 
                    newAngle[1] - prevAngle[1], 
                    newAngle[2] - prevAngle[2], 
                    newAngle[3] - prevAngle[3])) > 0.01f) doUpdate = true;
            }
            */
            //Debug.Log(string.Format("newAngle: {0}-{1}-{2}-{3}", newAngle[0], newAngle[1], newAngle[2], newAngle[3]));
            if (isRunning)
            {
                //counter++;
                // Change the status color of the button and target
                //MeshRenderer buttonRenderer = button.GetComponent<MeshRenderer>();
                //buttonRenderer.material.color = enabledColor;
                MeshRenderer targetRenderer = target.GetComponent<MeshRenderer>();
                targetRenderer.material.color = enabledColor;
                // Hide the refframe
                refPos.GetComponent<MeshRenderer>().enabled = false;
                button.GetComponentInChildren<MeshRenderer>().enabled = false;
                connect.GetComponentInChildren<MeshRenderer>().enabled = false;
                //robot.GetComponent<MeshRenderer>().enabled = false;

                if (Time.time - startTime > slot.GetComponent<GenerateSlot>().SegLength)
                {
                    startTime = Time.time;
                }
                float currentTime = Time.time - startTime; ;//connect.GetComponent<HololensUWPTcpClient>().CurrentTime;
                // Move the slot
                //Debug.Log(string.Format("newPosition: {0}, seglength: {1}",newPosition, slot.GetComponent<GenerateSlot>().SegLength));
                movingFrame.transform.position = startPosition - movingFrame.transform.up * currentTime;

                // Move the target
                float qd = slot.GetComponent<GenerateSlot>().GetDesiredTrajectory(currentTime);
                target.transform.position = startPosition + Vector3.left * qd;/*Vector3.Lerp(target.transform.position, 
                    startPosition + Vector3.right * targetPosition[0],// + Vector3.up * targetPosition[1], 
                    Mathf.SmoothStep(0,1,0.99f));*/

                /* Write timestamp and desired trajectory position rendered on AR display */
                string filepath = Path.Combine(Application.persistentDataPath, "qd_captured.txt");
                using (var stream = new FileStream(filepath, FileMode.Append, FileAccess.Write))
                {
                    var writer = new StreamWriter(stream);
                    writer.WriteLine(string.Format("{0},{1}", currentTime, qd));
                    writer.Flush();
                }
                
                //robot.transform.position = startPosition + Vector3.right * endEffectorPos[0];
                /*robot.transform.position = Vector3.Lerp(robot.transform.position, 
                    startPosition + Vector3.right * endEffectorPos[0],//+ Vector3.up * endEffectorPos[1];
                    Mathf.SmoothStep(0,1,0.99f));*/
                /*Debug.Log(string.Format("qd_robot_diff: {0}",
                                         targetPosition[0] - 
                                         slot.GetComponent<GenerateSlot>().GetDesiredTrajectory(currentTime)
                                         ));*/
            }
            else
            {
                // Show the refframe
                refPos.GetComponent<MeshRenderer>().enabled = true;
                button.GetComponentInChildren<MeshRenderer>().enabled = true;
                connect.GetComponentInChildren<MeshRenderer>().enabled = true;

                // Change the status color of the button and target
                MeshRenderer buttonRenderer = button.GetComponent<MeshRenderer>();
                MeshRenderer targetRenderer = target.GetComponent<MeshRenderer>();
                buttonRenderer.material.color = disabledColor;
                targetRenderer.material.color = disabledColor;

                // Reset the target position
                startTime = Time.time;
                targetPosition = new float[2] { 0, 0 };
                //target.transform.position = startPosition;
                movingFrame.transform.position = startPosition;
                //robot.transform.position = startPosition;
            }
            /*
            // Move the joint positions
            if (doUpdate)
            {
                for (int i = 0; i < joint.Length; i++)
                {
                    Vector3 anchor = joint[i].transform.TransformPoint(joint[i].GetComponent<HingeJoint>().anchor);
                    Vector3 axis = joint[i].transform.TransformDirection(joint[i].GetComponent<HingeJoint>().axis);
                    joint[i].transform.RotateAround(anchor, axis, (prevAngle[i] - newAngle[i]));
                    //Debug.Log(string.Format("newAngle{0}: {1}, prevAngle{2}: {3}", i, newAngle[i], i, prevAngle[i]));
                }
                prevAngle = newAngle;
                doUpdate = false;
            }
            */
        }
    }
}
